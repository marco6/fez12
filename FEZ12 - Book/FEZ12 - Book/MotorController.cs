using System;
using Microsoft.SPOT;
using Gadgeteer.Modules.GHIElectronics;
using Gadgeteer.SocketInterfaces;
using System.Threading;

namespace FEZ12___Book
{
    class MotorController
    {
        private readonly Extender extender;
        private readonly DigitalOutput stickRight, stickLeft, wheel;
        private bool right = false;

        public MotorController(Extender extender)
        {
            this.extender = extender;
            stickRight = extender.CreateDigitalOutput(Gadgeteer.Socket.Pin.Eight, false);
            stickLeft = extender.CreateDigitalOutput(Gadgeteer.Socket.Pin.Seven, false);
            wheel = extender.CreateDigitalOutput(Gadgeteer.Socket.Pin.Six, false);
        }

        public void ToggleStick()
        {
            stickLeft.Write(right);
            right = !right;
            stickRight.Write(right);
        }

        public void StickPowerOff()
        {
            stickLeft.Write(false);
            stickRight.Write(false);
        }

        public void WheelPowerOn()
        {
            wheel.Write(true);
        }

        public void WheelPowerOff()
        {
            wheel.Write(false);
        }
    }
}
