﻿using System;
using System.Collections;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Presentation;
using Microsoft.SPOT.Presentation.Controls;
using Microsoft.SPOT.Presentation.Media;
using Microsoft.SPOT.Presentation.Shapes;
using Microsoft.SPOT.Touch;

using Gadgeteer.Networking;
using GT = Gadgeteer;
using GTM = Gadgeteer.Modules;
using Gadgeteer.Modules.GHIElectronics;
using System.Net.Sockets;
using System.Net;
using Ws.Services.Binding;
using Dpws.Client;
using Ws.Services;
using System.Ext;
using Microsoft.SPOT.Net.NetworkInformation;
using tempuri.org;
using System.IO;


namespace FEZ12___Book
{
    public partial class Program
    {

        //private static readonly getPages req = new getPages();

        private IPhotoServiceClientProxy client;
        private DisplayController displayController;
        private uint pages = 0;
        private bool stopped = false;
        bool powerOff = true;
        private MotorController mc;
        // This method is run when the mainboard is powered up or reset.   
        void ProgramStarted()
        {
           
            Debug.Print("Program Started");

            ethernetJ11D.UseStaticIP("192.168.173.10", "255.255.255.0", "192.168.173.1");
            ethernetJ11D.NetworkUp += ethernetJ11D_NetworkUp;

            if (!ethernetJ11D.NetworkInterface.Opened)
                ethernetJ11D.NetworkInterface.Open();
            
            camera.PictureCaptured += new Camera.PictureCapturedEventHandler(event_picture);

            mc = new MotorController(extender);
        }

        private void ethernetJ11D_NetworkUp(GTM.Module.NetworkModule sender, GTM.Module.NetworkModule.NetworkState state)
        {
            client_init();
            displayController = new DisplayController(displayT35);
            displayController.OnStart += StartDigitalizzation;
            displayController.OnStop += StopDigitalizzation;
        }

        private void StopDigitalizzation()
        {
            stopped = true;
        }

        private void StartDigitalizzation()
        {
            pages = client.getPages(new getPages()).getPagesResult;
            if (pages == 0)
            {
                displayController.Finish();
                return;
            }

            displayController.TotPages = (int)pages;
            displayController.TakenPages = 0;
            displayController.Title = client.getTitle(new getTitle()).getTitleResult;
            camera.TakePicture();
        }

        private void client_init()
        {
            Uri url = new Uri("http://192.168.173.1:56235/PhotoService.svc");

            HttpTransportBindingConfig tbc = new HttpTransportBindingConfig(url);

            var binding = new WS2007HttpBinding(tbc);
            ProtocolVersion ver = new ProtocolVersion11();
            client = new IPhotoServiceClientProxy(binding, ver);
        }

        private void event_picture(Camera sender, GT.Picture picture)
        {
            displayController.Preview = picture.MakeBitmap();
            new Thread(() => Upload(picture)).Start();
        }

        private void Upload(GT.Picture picture)
        {
            var uploadReq = new upload();

            Debug.Print("Upload start");

            byte[] ll = new byte[8];
            // Open socket
            using (Socket sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
            {
                sock.Connect(new IPEndPoint(new IPAddress(new byte[] { 192, 168, 173, 1 }), 56236));
                Debug.Print("Connected!");
                sock.Receive(ll);
                sock.Send(picture.PictureData);
                Debug.Print("Data sent!");

            }
            uploadReq.id = BitConverter.ToInt64(ll, 0);
            Debug.Print("Id is:" + uploadReq.id);
            
            if (client.upload(uploadReq).uploadResult == false) {
                Debug.Print("Impossibile caricare immagine.");
            }
            Debug.Print("DONE!");
            uploadFinished();
        }

        public void uploadFinished()
        {
            Program.Invoke += delegate
            {
                displayController.TakenPages++;
            };
            if (stopped || --pages == 0)
            {
                Program.Invoke += delegate
                {
                    displayController.Finish();
                };
                stopped = false;
                pages = 0;
                return;
            }
            TurnPage();
            camera.TakePicture();
        }

        private void TurnPage()
        {
            mc.WheelPowerOn();
            
            Thread.Sleep(850);// Tempo di rotazione per alzare la pagina

            mc.WheelPowerOff();

            Thread.Sleep(300);// Tempo per far fermare la ruota
            
            mc.ToggleStick();

            Thread.Sleep(1150);// Tempo di rotazione per girare la pagina
            
            mc.ToggleStick();

            Thread.Sleep(2800);// Tempo di rotazione per tornare in posizione            

            mc.StickPowerOff();
        }
    }
}