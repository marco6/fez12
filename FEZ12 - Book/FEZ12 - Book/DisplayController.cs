using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Presentation;
using Microsoft.SPOT.Presentation.Controls;
using System.IO;
using Microsoft.SPOT.Presentation.Media;

namespace FEZ12___Book
{
    class DisplayController
    {

        public delegate void Handler();

        private int totPages;
        private Text title;
        private Border progressBar;
        private Image preview;
        private int takenPages;

        private Window mainWindow;
        private Gadgeteer.Modules.GHIElectronics.DisplayT35 displayT35;

        private UIElement startPage, workingPage;

        public event Handler OnStart, OnStop;

        public DisplayController(Gadgeteer.Modules.GHIElectronics.DisplayT35 display)
        {
            mainWindow = display.WPFWindow;
            
            setupStartPage();
            setupWorkingPage();
            mainWindow.Child = startPage;
        }

        private void setupWorkingPage()
        {
            StackPanel stackPanel = new StackPanel();
            stackPanel.SetMargin(10);

            title = new Text(Resources.GetFont(Resources.FontResources.NinaB), "TITOLO");
            title.Width = mainWindow.Width;
            title.TextAlignment = TextAlignment.Center;
            title.TextWrap = true;
            title.SetMargin(10);

            stackPanel.Children.Add(title);

            Border border = new Border();
            border.Background = new SolidColorBrush(Colors.White);
            border.BorderBrush = new SolidColorBrush(Colors.Black);

            progressBar = new Border();
            progressBar.HorizontalAlignment = HorizontalAlignment.Left;
            progressBar.Width = 100;
            progressBar.Height = 30;
            progressBar.SetMargin(2);
            progressBar.Background = new SolidColorBrush(Colors.Blue);

            border.Child = progressBar;

            stackPanel.Children.Add(border);

            StackPanel horizontal = new StackPanel(Orientation.Horizontal);
            horizontal.SetMargin(0, 10, 0, 10);

            border = new Border();
            border.Background = new SolidColorBrush(Colors.White);
            border.BorderBrush = new SolidColorBrush(Colors.Black);

            preview = new Image();
            preview.Width = 180;
            preview.Height = 135;

            border.Child = preview;
            border.HorizontalAlignment = HorizontalAlignment.Right;
            horizontal.Children.Add(border);


            border = new Border();
            border.Background = new SolidColorBrush(Colors.Red);
            border.BorderBrush = new SolidColorBrush(Colors.Black);
            border.VerticalAlignment = VerticalAlignment.Center;
            border.SetMargin(30);
            border.TouchUp += Stop;
            //Button

            Text text = new Text();
            text.Font = Resources.GetFont(Resources.FontResources.NinaB);

            text.TextContent = "STOP";

            text.SetMargin(15);
            text.ForeColor = Color.Black;

            border.Child = text;
            horizontal.Children.Add(border);

            stackPanel.Children.Add(horizontal);
            workingPage = stackPanel;
        }

        private void Stop(object sender, Microsoft.SPOT.Input.TouchEventArgs e)
        {
            if(OnStop != null)
                OnStop();
        }

        private void setupStartPage()
        {
            StackPanel stackPanel = new StackPanel(Orientation.Vertical);

            Border border = new Border();
            border.Background = new SolidColorBrush(Colors.Green);
            border.BorderBrush = new SolidColorBrush(Colors.Black);
            border.TouchUp += Start;
            //Button

            Text text = new Text();
            text.Font = Resources.GetFont(Resources.FontResources.NinaB);

            text.TextContent = "START";

            text.SetMargin(50);
            text.TextWrap = true;
            text.ForeColor = Color.Black;

            border.Child = text;
            stackPanel.Children.Add(border);

            stackPanel.HorizontalAlignment = HorizontalAlignment.Center;
            stackPanel.VerticalAlignment = VerticalAlignment.Center;
            stackPanel.SetMargin(5);

            startPage = stackPanel;
        }

        private void Start(object sender, Microsoft.SPOT.Input.TouchEventArgs e)
        {
            mainWindow.Child = workingPage;
            if(OnStart != null)
                OnStart();
        }

        public void Finish()
        {
            mainWindow.Child = startPage;
        }

        public Bitmap Preview
        {
            get { return preview.Bitmap; }
            set
            {
                preview.Bitmap = value;
           }
        }

        public int TakenPages
        {
            get { return takenPages; }
            set
            {
                takenPages = value;
                progressBar.Width = (mainWindow.Width - 26)* takenPages / totPages;
            }
        }
        public int TotPages
        {
            get { return totPages; }
            set
            {
                totPages = value;
            }
        }

        public string Title
        {
            get { return title.TextContent; }
            set
            {
                title.TextContent = value;
            }
        }
    }
}
