﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace PhotoService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IPhotoService
    {
        [OperationContract]
        String getBookPage(String id);

        [OperationContract]
        String[] getBooksIDs();

        [OperationContract]
        bool setTitle(String title);

        [OperationContract]
        string getTitle();

        [OperationContract]
        bool setPages(uint start, uint end);

        [OperationContract]
        uint getPages();

        [OperationContract]
        bool upload(long id);

    }
}
