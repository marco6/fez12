﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Web;

namespace PhotoService
{
    public class Uploader
    {
        private Socket ListenSocket;
        private IPEndPoint localEP;
        private Thread myThread;
        
        public Uploader()
        {
            ListenSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            ListenSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            localEP = new IPEndPoint(new IPAddress(new byte[] {192, 168, 173, 1}), 56236);
            ListenSocket.Bind(localEP);
            ListenSocket.Listen(10);
            myThread = new Thread(StartWatch);
            myThread.Start();
        }

        private void StartWatch()
        {
            while (true)
            {
                using (var ns = new NetworkStream(ListenSocket.Accept()))
                {
                    using (BinaryWriter bw = new BinaryWriter(ns))
                    {
                        long now = DateTime.Now.Ticks;
                        bw.Write(now);
                        Image img = Image.FromStream(ns);
                        img.Save(@"C:\Users\mrcmn\workspace\fez12\FEZ12 - Book\PhotoService\Images\" + now + ".jpg",
                            System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                }
            }
        }
    }
}