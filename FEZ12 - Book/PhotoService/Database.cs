﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PhotoService
{
    public class Database
    {
        private static readonly string db_path = @"Data Source=(LocalDB)\v11.0;AttachDbFilename='C:\Users\mrcmn\workspace\fez12\FEZ12 - Book\PhotoService\App_Data\database.mdf';Integrated Security=True";
        private static SqlConnection con;

        static Database()
        {
            con = new SqlConnection(db_path);
            con.Open();
        }

        public static void Insert(string ID, string REF)
        {
            try
            {
                using (SqlCommand command = new SqlCommand(
                    "INSERT INTO Pages VALUES(@ID, @REF)", con))
                {
                    command.Parameters.Add(new SqlParameter("ID", ID));
                    command.Parameters.Add(new SqlParameter("REF", REF));
                    command.ExecuteNonQuery();
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(" DATABASE : Element not inserted.");
            }
        }

        public static Dictionary<string, string> Load()
        {
            Dictionary<string, string> ret = new Dictionary<string, string>();
            try
            {
                using (SqlCommand command = new SqlCommand("SELECT * FROM Pages", con))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string ID = reader.GetString(0);
                            string REF = reader.GetString(1);
                            ret.Add(ID, REF);
                        }
                    }
                }
            }
            catch { }

            return ret;
        }
    }
}