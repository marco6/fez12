﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Text;
using System.Threading;

namespace PhotoService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class PhotoService : IPhotoService
    {

        static PhotoService() {
            up = new Uploader();
            db = Database.Load();
        }

        private static Uploader up;
        private static uint startPage = 0, endPage = 0;
        private static String currentTitle = "";
        private static Dictionary<string, string> db;

        public string getBookPage(string id)
        {
            return db[id];
        }

        public string[] getBooksIDs()
        {
            string[] keys = new string[db.Keys.Count];
            db.Keys.CopyTo(keys, 0);
            return keys;
        }

        public bool setTitle(string title)
        {
            currentTitle = title;
            return true;
        }

        public bool setPages(uint start, uint end)
        {
            if (start > end)
                throw new ArgumentOutOfRangeException();
            startPage = start;
            endPage = end;
            return true;
        }

        public uint getPages()
        {
            return endPage - startPage + 1;
        }

        public bool upload(long id)
        {
            if (getPages() == 0)
                return false;
            string ID = currentTitle + " - p. " + startPage++, 
                REF = string.Format(@"{0}.jpg", id);
            db.Add(ID, REF);
            Database.Insert(ID, REF);
            return true;
        }

        public string getTitle()
        {
            return currentTitle;
        }
    }
}
